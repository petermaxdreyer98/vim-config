set nocompatible              " be iMproved, required
filetype off                  " required
set background=dark
syntax on
set number
set tabstop=2
set laststatus=2
set nowrap

" Hide gui elements
set guioptions-=m " Remove top menu
set guioptions-=T " Remove toolbar
set guioptions-=r " Remove right-hand scroll bar
set guioptions-=L " Remove left-hand scroll bar.

let g:netrw_liststyle=3
let g:netrw_banner=0
let g:netrw_browse_split=4
let g:netrw_winsize=25

let g:ale_fixers = {
\ 'javascript': ['eslint'],
\ 'typescript': ['eslint', 'tslint', 'tsserver'],
\}

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'solarized'
Plugin 'tpope/vim-fugitive'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
Plugin 'w0rp/ale'
Plugin 'wavded/vim-stylus'
Plugin 'othree/jsdoc-syntax.vim'

" Make CtrlP ignore all node modules
let g:ctrlp_user_command = 'find %s -type f | grep -v "node_modules/" | grep -v ".git/"'

set expandtab
set shiftwidth=2

" Typescript Support
Plugin 'leafgarland/typescript-vim'

augroup typescript_support
	au!
	autocmd BufEnter *.ts   set syntax=typescript
	autocmd BufEnter *.ts   setfiletype typescript
  autocmd BufEnter *.ts   let g:ale_completion_enabled = 1
augroup END

augroup stylus_support
  au!
  autocmd BufEnter *.styl   set syntax=stylus
augroup END

" Vim tab command completion
set wildmode=longest,list,full
set wildmenu

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" Code Folding
set foldmethod=syntax
set foldcolumn=1
let javaScript_fold=1
set foldlevelstart=99

" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ

colorscheme solarized
